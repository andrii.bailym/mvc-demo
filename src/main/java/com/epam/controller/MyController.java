package com.epam.controller;

import com.epam.model.JsonExample;
import com.epam.model.LoginDto;
import com.epam.model.UserDto;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes(value = "userInfo")
public class MyController {

  private static final String USER_FORM = "userForm";

  @GetMapping(value = "/json-example")
  @ResponseBody
  public JsonExample getJsonExample() {
    return new JsonExample("TEST");

  }

  @GetMapping(value = "/login")
  public ModelAndView showLogin() {
    return new ModelAndView("index", USER_FORM, new LoginDto());

  }

  @PostMapping(value = "/signin")
  public ModelAndView login(@Valid @ModelAttribute(USER_FORM) LoginDto loginDto,
      BindingResult result) {
    if (result.hasErrors()) {
      return new ModelAndView("index");
    }

    if ("admin@email.com".equals(loginDto.getEmail()) && "admin".equals(loginDto.getPassword())) {
      ModelAndView mav = new ModelAndView();
      final UserDto userDto = new UserDto(1, "admin", "admin");
      mav.setViewName("current-user-info");
      mav.addObject("userInfo", userDto);
      return mav;
    } else {
      throw new RuntimeException("Wrong password-email combination");
    }

  }


}
