package com.epam.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Slf4j
public class ExceptionResolver {

  @ExceptionHandler({Throwable.class})
  ModelAndView handleAllExceptions(Throwable ex) {
    log.error("Error message: {}", ex.getMessage());
    return new ModelAndView("error", "errorMessage", ex.getMessage());
  }

}
