package com.epam.model;

import lombok.Data;

@Data
public class UserDto {
  private final long id;
  private final String username;
  private final String role;
}
