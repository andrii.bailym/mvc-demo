package com.epam.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class LoginDto {

  @NotEmpty
  @Email
  private String email;

  @NotEmpty(message = "Please enter your password.")
  @Size(min = 4, max = 15, message = "Your password must between 4 and 15 characters")
  private String password;
}
